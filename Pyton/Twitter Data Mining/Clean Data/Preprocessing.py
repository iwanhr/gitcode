import re
import nltk
import pandas as pd

def casefolding (review):
    review = review.lower()
    return review

def tokenize(review):
    token = nltk.word_tokenize(review)
    return token

def filtering(review):
    #hapus angka dan angka yang berada dalam string
    #Removing non ASCII chars
    review = re.sub(r'[^\x00-\x7f]', r'', review)
    review = re.sub(r'(\\u[0-9A-Fa-f]+)', r'', review)
    review = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", review)
    review = re.sub(r'\\u\w\w\w\w', '', review)
    #Menghapus link web
    review = re.sub(r'http\S+', '', review)
    # hapus @username
    review = re.sub('@[^\s]+', '', review)
    # hapus #tagger
    review = re.sub(r'#([^\s]+)', '', review)
    # hapus simbol, angka dan karakter aneh
    review = re.sub(r"[.,:;+!\-_<^/=?\"'\(\)\d\*]", " ", review)
    return review

def hapus_reviewdouble(review):
   # look for 2 or more repetitions of character and replace with the character itself
   pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
   return pattern.sub(r"\1\1", review)

def konversi_slangword(review):
    kamus_slangword = eval(open("slangwords.txt").read())#Membuka dictionasry slangword
    pattern = re.compile(r'\b( ' + '|'.join (kamus_slangword.keys())+r')\b')#untuk mencari pola kara contoh tdk menjadi tidak
    content = []
    for kata in review:
        filteredSlang = pattern.sub(lambda x: kamus_slangword[x.group()],kata)#mengganti slangwsd berdasarkan pola review yg telah ditentukan
        content.append(filteredSlang.lower())
    review = content
    return review

def hapusStopword(review):
    stopwords = open('stopword-tala.txt', 'r').read().split()
    content = []
    #for w in review:
    filteredtext = [word for word in review.split() if word not in stopwords]
    content.append(" ".join(filteredtext))
    review = content
    return review


data = pd.read_csv('#gempa', encoding='latin-1')
# Keeping only the neccessary columns
datasets = [data]
print(data)

for teks in datasets:
    label = teks['sentiment']
    teks = teks['text'].apply(casefolding)
    teks = teks.apply(filtering)
    teks = teks.apply(hapus_reviewdouble)
    teks = teks.apply(tokenize)
    teks = teks.apply(konversi_slangword)
    teks = teks.apply(" ".join)
    teks = teks.apply(hapusStopword)
    teks = teks.apply(" ".join)
    print(teks)

review_dict = {'text': teks, 'sentiment' : label}
df = pd.DataFrame(review_dict, columns = ['text', 'sentiment'])
print(df.info())
df.to_csv('sultraClean.csv', sep= ',' , encoding='utf-8')